/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalall <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/29 16:32:02 by agalall           #+#    #+#             */
/*   Updated: 2021/09/29 16:32:20 by agalall          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*p;

	if (count == 0 || size == 0)
		return (NULL);
	p = malloc(count * size);
	ft_memset(p, 0, count * size);
	return (p);
}
/*
int main(void)
{
	int	*p;

	p = (int *) ft_calloc(10, sizeof(int));
	p[8] = 128;
	printf("%d\n", p[7]);
	printf("%d\n", p[8]);
	printf("%d\n", p[9]);
}
*/
