/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalall <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/29 16:36:50 by agalall           #+#    #+#             */
/*   Updated: 2021/09/29 16:37:12 by agalall          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	str1;
	size_t	str2;
	size_t	len;
	char	*s;

	str1 = ft_strlen(s1);
	str2 = ft_strlen(s2);
	len = str1 + str2;
	s = (char *) malloc(len + 1);
	if (!s)
		return (NULL);
	ft_memcpy(s, s1, str1);
	ft_strlcat(s, s2, len + 1);
	return (s);
}
/*int main(void)
{
	char str1[] = "abcdef";
	char str2[] = "ghijkl";

	printf("%s\n", ft_strjoin(str1, str2));
}
*/
