/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalall <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/29 16:35:18 by agalall           #+#    #+#             */
/*   Updated: 2021/10/05 20:46:30 by agalall          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	char	*result;

	result = (char *) malloc(len + 1);
	if (!result)
		return (NULL);
	i = 0;
	while (s[start] && i < len)
	{
		result[i++] = s[start++];
	}
	result[len] = '\0';
	return (result);
}
/*int main(void)
{
	char str[] = "abcdefghijklmnop";
	printf("%s", ft_substr(str, 4, 5));
}
*/
