/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalall <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/29 16:33:47 by agalall           #+#    #+#             */
/*   Updated: 2021/09/29 16:33:59 by agalall          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*s2;
	size_t	size;

	if (!s1)
		return (NULL);
	size = ft_strlen(s1);
	s2 = (char *) malloc (size * sizeof(char));
	if (!s1)
		return (NULL);
	ft_memcpy(s2, s1, size);
	return (s2);
}
/*
int main(void)
{
	char *str = "Helloworld";
	char *result;

	result = ft_strdup(str);
	printf("The string : %s\n", result);
	return (0);
}
*/
