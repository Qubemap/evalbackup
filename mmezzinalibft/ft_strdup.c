/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:15:54 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/10 13:15:55 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strdup(const char *src)
{
	char	*dup;
	size_t	len;

	len = ft_strlen(src) + 1;
	dup = malloc(sizeof(char) * len);
	if (!dup)
	{
		return (0);
	}
	dup = ft_memcpy(dup, src, len);
	return (dup);
}

//return pointer to new string which is duplicate of string 'src'