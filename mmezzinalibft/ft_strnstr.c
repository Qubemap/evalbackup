/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:16:41 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/10 13:16:42 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strnstr(const char *haystack, const char *needle, size_t num)
{
	size_t	i;
	size_t	j;
	size_t	lenneed;
	char	*h;

	i = 0;
	h = (char *)haystack;
	lenneed = ft_strlen(needle);
	if (lenneed == 0 || haystack == needle)
		return (h);
	while (h[i] != '\0' && i < num)
	{
		j = 0;
		while (h[i + j] != '\0' && needle[j] != '\0' && h[i + j] == needle[j]
			&& i + j < num)
			j++;
		if (j == lenneed)
			return (h + i);
		i++;
	}
	return (0);
}

//finds first occurrence of substring 'needle' in string 'haystack'
//doesn't compare null-term chars
//only searches 'num' amount of chars