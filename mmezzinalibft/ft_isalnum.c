/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:24:21 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/10 13:24:23 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isalnum(int m)
{
	int	isalnum;

	if ((m >= '0' && m <= '9') || (m >= 'A' && m <= 'Z')
		|| (m >= 'a' && m <= 'z'))
	{
		isalnum = 1;
	}
	else
	{
		isalnum = 0;
	}
	return (isalnum);
}

//check if arg is alphabet, number, or neither: