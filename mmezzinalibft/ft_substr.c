/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:21:06 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/10 13:21:07 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*ret;
	size_t	i;

	if (!s)
		return (NULL);
	if ((size_t)start > ft_strlen(s))
		return (ft_strdup(""));
	ret = malloc(sizeof(char) * (len + 1));
	i = 0;
	if (!ret)
		return (0);
	while (i < len)
	{
		ret[i] = *(s + start + i);
		i++;
	}
	ret[i] = '\0';
	return (ret);
}

//using malloc, return substring from string 's'
//begins at index 'start' andis of max length 'len'