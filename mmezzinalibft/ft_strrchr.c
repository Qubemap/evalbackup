/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:20:41 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/10 13:20:52 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strrchr(const char *str, int c)
{
	int		i;
	char	*ptr;

	i = 0;
	ptr = 0;
	while (str[i] != '\0')
	{
		if (str[i] == c)
		{
			ptr = (char *)(str + i);
		}
		i++;
	}
	if (str[i] == c)
	{
		ptr = (char *)(str + i);
	}
	return (ptr);
}

//finds last occurrence of char 'c' in string 'str'