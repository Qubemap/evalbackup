/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:18:55 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/10 13:18:56 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char		*dstt;
	const char	*srcc;

	if ((dst == src) || n == 0)
	{
		return (dst);
	}
	if (!dst && !src)
	{
		return (0);
	}
	dstt = (char *)dst;
	srcc = (const char *)src;
	while (n--)
	{
		dstt[n] = srcc[n];
	}
	return (dst);
}

//copy 'n' chars from 'src' to 'dst'