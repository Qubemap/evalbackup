/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:17:30 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/16 13:48:10 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isalpha(int m)
{
	if ((m >= 'A' && m <= 'Z') || (m >= 'a' && m <= 'z'))
		return (1);
	return (0);
}

//check if arg is alphabet
