/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmezzina <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 13:18:29 by mmezzina          #+#    #+#             */
/*   Updated: 2021/11/10 13:18:31 by mmezzina         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isprint(int m)
{
	int	isprint;

	if (m >= 32 && m <= 126)
	{
		isprint = 1;
	}
	else
	{
		isprint = 0;
	}
	return (isprint);
}

//check if arg is printable character