#include "ft_printf.h"

int	ft_printf(const char *str, ...)
{
	va_list	args;
	int		(*f)(const char *, va_list);
	int		i;

	f = &vprintf;
	va_start(args, str);
	i = (*f)(str, args);
	va_end(args);
	return (i);
}
