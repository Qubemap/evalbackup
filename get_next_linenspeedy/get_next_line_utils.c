/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nspeedy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/30 08:56:18 by nspeedy           #+#    #+#             */
/*   Updated: 2021/10/14 15:49:21 by nspeedy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"get_next_line.h"

size_t	gnl_strlen(const char *s)
{
	size_t	len;

	len = 0;
	while (s[len] != '\0')
		len++;
	return (len);
}

void	*gnl_memcpy(void *dest, const void *src, size_t n)
{
	unsigned char		*destp;
	const unsigned char	*srcp;

	if (!dest && !src)
		return (0);
	destp = dest;
	srcp = src;
	while (n > 0)
	{
		n--;
		*destp++ = *srcp++;
	}
	return (dest);
}

size_t	gnl_strlcpy(char *dest, const char *src, size_t dstsize)
{
	size_t	srclen;

	srclen = gnl_strlen(src);
	if (srclen + 1 < dstsize)
	{
		gnl_memcpy(dest, src, srclen);
		dest[srclen] = '\0';
	}
	else if (dstsize != 0)
	{
		gnl_memcpy(dest, src, dstsize - 1);
		dest[dstsize - 1] = '\0';
	}
	return (srclen);
}

size_t	gnl_strlcat(char *dest, const char *src, size_t size)
{
	size_t	srclen;
	size_t	destlen;

	srclen = gnl_strlen(src);
	destlen = gnl_strlen(dest);
	if (destlen > size)
		destlen = size;
	if (destlen == size)
		return (size + srclen);
	if (srclen < size - destlen)
	{
		gnl_memcpy(dest + destlen, src, srclen);
		dest[destlen + srclen] = '\0';
	}
	else
	{
		gnl_memcpy(dest + destlen, src, size - destlen - 1);
		dest[size - 1] = '\0';
	}
	return (destlen + srclen);
}

char	*gnl_strjoin(char const *s1, char const *s2)
{
	size_t	slen1;
	size_t	slen2;
	char	*dest;

	if (!s1 || !s2)
		return (0);
	slen1 = gnl_strlen(s1);
	slen2 = gnl_strlen(s2);
	dest = (char *)malloc(sizeof(char) + (slen1 + slen2 + 1));
	if (!dest)
		return (0);
	gnl_strlcpy(dest, s1, slen1 + 1);
	gnl_strlcat(dest + slen1, s2, slen2 + 1);
	return (dest);
}

char	*gnl_strdup(const char *s)
{
	size_t	len;
	char	*scpy;

	len = gnl_strlen(s);
	scpy = (char *)malloc(sizeof(char) * (len + 1));
	if (!scpy)
		return (0);
	len = 0;
	while (s[len])
	{
		scpy[len] = s[len];
		len++;
	}
	scpy[len] = '\0';
	return (scpy);
}

char	*gnl_strchr(const char *s, int c)
{
	while (*s != c)
	{
		if (*s == '\0')
			return (0);
		s++;
	}
	return ((char *)s);
}

char	*gnl_substr(char const *s, unsigned int start, size_t len)
{
	char	*substr;

	if (s == 0)
		return (0);
	if ((unsigned int)gnl_strlen(s) < start)
		return (gnl_strdup(""));
	substr = (char *)malloc(sizeof(char) * (len + 1));
	if (!substr)
		return (0);
	gnl_strlcpy(substr, s + start, len + 1);
	return (substr);
}
