/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nspeedy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/30 08:56:56 by nspeedy           #+#    #+#             */
/*   Updated: 2021/10/14 16:24:04 by nspeedy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef	GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include	<stdlib.h>
# include	<unistd.h>
# include	<limits.h>

# ifndef	BUFFER_SIZE
#	define	BUFFER_SIZE 1
# endif

# ifndef	OPEN_MAX
#	define	OPEN_MAX	10240
# endif

char	*get_next_line(int fd);
size_t	gnl_strlen(const char *s);
void	*gnl_memcpy(void *dest, const void *src, size_t n);
size_t	gnl_strlcpy(char *dest, const char *src, size_t dstsize);
size_t	gnl_strlcat(char *dest, const char *src, size_t n);
char	*gnl_strjoin(char const *s1, char const *s2);
char	*gnl_strdup(const char *s);
char	*gnl_strchr(const char *s, int c);
char	*gnl_substr(char const *s, unsigned int start, size_t len);

#endif
