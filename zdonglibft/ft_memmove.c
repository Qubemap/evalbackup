/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:39:30 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:39:32 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*d;
	char	*s;

	d = (char *)dst;
	s = (char *)src;
	if (dst == src)
		return (dst);
	if (!d && !s)
		return (NULL);
	if (d > s)
		while (len--)
			*(d + len) = *(s + len);
	else
		while (len--)
			*d++ = *s++;
	return (dst);
}
/*
#include <stdio.h>

int	main(void)
{
	char	dst[] = "abcdefgh";
	char	src[] = "123456";
	int		len = 3;
	ft_memmove(dst, src, len);
	printf("%s", dst);
}
*/
