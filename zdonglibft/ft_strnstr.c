/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:45:06 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:45:08 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	int		i;
	int		j;
	char	*p1;
	char	*p2;

	p1 = (char *)haystack;
	p2 = (char *)needle;
	i = 0;
	if (p2[i] == '\0')
		return (p1);
	while (i < len)
	{
		j = 0;
		while (p1[i + j] == p2[j] && (i + j) < len && p1[i + j])
			j++;
		if (p2[j] == '\0')
			return (&p1[i]);
		i++;
	}
	return (0);
}
/*
#include <stdio.h>
int	main(void)
{
	char *a = "A";
	char *b = "A";
	a = ft_strnstr(a, b, 2);
	printf("%s", a);
}
*/
