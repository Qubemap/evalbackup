/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 15:32:18 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:41:12 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	split_count(char const *s, char c)
{
	int	i;
	int	count;

	i = 0;
	count = 0;
	while (s[i])
	{
		if (s[i] != c)
		{
			if (i == 0)
				count++;
			else if (s[i] != c && s[i - 1] == c)
				count++;
		}
		i++;
	}
	return (count);
}

char	*get_substring(char const *s, char c, size_t index)
{
	size_t	start;
	size_t	len;

	start = 0;
	len = 0;
	while (s[start] == c)
		start++;
	while (index > 0)
	{
		start++;
		if (s[start] != c && s[start - 1] == c)
			index--;
	}
	while ((s[start + len] != '\0') && (s[start + len] != c))
		len++;
	return (ft_substr(s, start, len));
}

char	**ft_split(char const *s, char c)
{
	char	**dst;
	size_t	i;
	size_t	splits;

	i = 0;
	splits = split_count(s, c);
	dst = (char **)malloc(sizeof(char **) * (splits + 1));
	if (!dst)
		return (NULL);
	while (i < splits)
	{
		dst[i] = get_substring(s, c, i);
		i++;
	}
	dst[i] = 0;
	return (dst);
}
