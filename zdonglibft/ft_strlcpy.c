/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:43:04 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:43:06 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *restrict dst, const char *restrict src, size_t dstsize)
{
	size_t	src_size;
	int		i;
	char	*source;

	source = (char *)src;
	i = 0;
	src_size = ft_strlen(source);
	if (!src)
	{	
		*dst = '\0';
		return (0);
	}
	if (!dstsize)
		return (src_size);
	while (src[i] && i < (dstsize - 1))
	{
		dst[i] = src[i];
		i++;
	}
	if (dstsize < src_size)
		dst[dstsize - 1] = '\0';
	else
		dst[i] = '\0';
	return (src_size);
}
