/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:34:50 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:34:53 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t n)
{
	char	*p;
	int		i;

	i = 0;
	p = s;
	if (n >= 0)
	{
		p = s;
		while (n > 0)
		{
			p[i] = 0;
			i++;
			n--;
		}
	}
}
/*
#include <stdio.h>
int	main(void)
{
	char s[] = "abcdefsljalkfjsl";
	int	n = 0;
	ft_bzero(s, n);
	printf("%s", s);
}
*/
