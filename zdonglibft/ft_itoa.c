/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:37:32 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:37:35 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_numb(long int n)
{
	int	nbr;

	if (n == 0)
		nbr = 1;
	else
	{
		nbr = 0;
		while (n > 0)
		{
			nbr++;
			n = n / 10;
		}
	}
	return (nbr);
}

char	*ft_allo(char *str, int nb, int sign, long int num)
{
	str[nb + sign] = '\0';
	if (num == 0)
		str[0] = '0';
	while (num > 0)
	{
		str[nb + sign - 1] = (num % 10) + '0';
		num = num / 10;
		nb--;
	}
	if (sign == 1)
		str[0] = '-';
	return (str);
}

char	*ft_itoa(int n)
{
	int			nb;
	long int	num;
	int			sign;
	char		*str;

	sign = 0;
	num = n;
	if (n < 0)
	{
		num = -num;
		sign = 1;
	}
	nb = ft_numb(num);
	str = (char *)malloc(sizeof(char) * (nb + sign + 1));
	if (!str)
		return (0);
	str = ft_allo(str, nb, sign, num);
	return (str);
}
/*
#include <stdio.h>
int	main(void)
{
	int a = -2147483648;
	char *p;

	p = ft_itoa(a);
	printf("%s", p);
}
*/
