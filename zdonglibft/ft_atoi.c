/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:34:04 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:34:19 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	int		i;
	int		flag;
	long	num;

	num = 0;
	i = 0;
	flag = 0;
	while (str[i] && (str[i] == 32 || str[i] == '\t' || str[i] == '\n'
			|| str[i] == '\r' || str[i] == '\v' || str[i] == '\f'))
		i++;
	if (str[i] == '-' && str[i])
	{
		flag = 1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	while (str[i] && ft_isdigit(str[i]))
		num = (str[i++] - '0') + num * 10;
	if (flag == 1)
		return (-num);
	return (num);
}
/*
#include <stdio.h>
int	main(void)
{
	char *str = "  +++3256";
	int	a;

	a = ft_atoi(str);
	printf("%d", a);
}
*/
