/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:41:27 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:41:29 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strchr(const char *s, int c)
{
	char	*p;

	p = (char *)s;
	while (*p)
	{
		if (*p == c)
			return (p);
		p++;
	}
	if (*p == c)
		return (p);
	return (0);
}
/*
#include <stdio.h>
int	main(void)
{
	char	*s = "abcdfegh";
	int 	c = 'c';
	char	*b;

	b = ft_strchr(s, c);
	printf("%s", b);
}
*/
