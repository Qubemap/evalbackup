/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:36:45 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:36:49 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isascii(int a)
{
	if (a >= 0 && a < 128)
		return (1);
	return (0);
}

/*
#include <stdio.h>
int	main(void)
{
	char	a;
	int	test;

	a = 'd';
	test = isascii(a);
	printf("%d", test);
}
*/
