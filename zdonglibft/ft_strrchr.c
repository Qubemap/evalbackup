/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:45:38 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:45:40 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*p;
	char	*p1;

	p1 = 0;
	p = (char *)s;
	while (*p)
	{
		if (*p == c)
			p1 = p;
		p++;
	}
	if (*p == c)
		p1 = p;
	return (p1);
}
/*
#include <stdio.h>
int	main(void)
{
	char	*s = "abcdficegh";
	int 	c = 'c';
	char	*b;

	b = ft_strrchr(s, c);
	printf("%s", b);
}
*/
