/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:35:51 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:35:54 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isalnum(int a)
{
	if ((a >= 'A' && a <= 'Z') || (a >= 'a' && a <= 'z')
		|| (a >= '0' && a <= '9'))
		return (1);
	return (0);
}
/*
#include <stdio.h>
int	main(void)
{
	char	a;
	int	test;

	a = '+';
	test = ft_isalnum (a);
	printf("%d", test);
}
*/
