/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 15:46:03 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:46:03 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	set_check(char c, char const *set)
{
	while (*set)
		if (c == *set++)
			return (1);
	return (0);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*p;
	int		start;
	int		end;
	int		i;

	if (!s1)
		return (0);
	if (!set)
		return ((char *)s1);
	start = 0;
	while (s1[start] && set_check(s1[start], set))
		start++;
	end = ft_strlen((char *)s1);
	while (end > start && set_check(s1[end - 1], set))
		end--;
	p = (char *)malloc (sizeof(char) * (end - start + 1));
	i = 0;
	while ((start + i) < end)
	{
		p[i] = s1[start + i];
		i++;
	}
	p[i] = '\0';
	return (p);
}
