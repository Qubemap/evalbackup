/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:42:28 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:42:30 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*p;
	int		len;
	int		i;
	int		j;

	len = ft_strlen((char *)s1) + ft_strlen((char *)s2) + 1;
	p = (char *)malloc(sizeof(char) * len);
	if (!p)
		return (0);
	i = 0;
	j = 0;
	while (s1[i])
		p[j++] = s1[i++];
	i = 0;
	while (s2[i])
		p[j++] = s2[i++];
	p[j] = '\0';
	return (p);
}

/*
#include <stdio.h>
int	main(void)
{
	char	*s1 = "abcdef";
	char	*s2 = "ghmnop";
	char	*p;

	p = ft_strjoin(s1, s2);
	printf ("%s", p);
}
*/
