/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zdong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:36:19 by zdong             #+#    #+#             */
/*   Updated: 2021/10/28 15:36:21 by zdong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isalpha(int chars)
{
	if ((chars >= 'A' && chars <= 'Z') || (chars >= 'a' && chars <= 'z'))
		return (1);
	return (0);
}

/*
#include <stdio.h>
int	main(void)
{
	char	a;
	int	test;

	a = '0';
	test = isalpha (a);
	printf("%d", test);
}
*/
